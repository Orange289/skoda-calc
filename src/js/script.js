(function($) {
$(function() {
	$('select').styler({
		onFormStyled: function() {
			$('.jq-selectbox__select').css({
				'background':'#ffffff',
				'border-color':'#dddddd',
				'box-shadow':'none',
				'height':'38px',
				'padding-top':'8px',
				'padding-left':'17px',
				'border-radius':'3px',
				'font':'400 14px/32px SkodaPro, sans-serif'
			});
			$('.jq-selectbox__select-text').css({
				'width':'100%',
				'text-align':'left'
			});
			$('.jq-selectbox__trigger').css('border-left', 'none');
			$('.jq-selectbox__dropdown').css({
				'text-align':'left',
				'top':'30px',
				'padding-left':'8px',
				'padding-top':'5px',
				'font':'400 14px/24px SkodaPro, sans-serif',
				'border':'none'
			});
		},
		onSelectOpened: function() {
			$(this).find('.jq-selectbox__trigger-arrow').css({
				'border-top-color':'#10b2ee',
				'transform':'rotate(135deg)',
				'top':'20px'
			})
		},
		onSelectClosed: function() {
			$(this).find('.jq-selectbox__trigger-arrow').css({
				'position':'absolute',
				'top':'16px',
				'right':'15px',
				'width': '9px',
				'height':'9px',
				'border-bottom':'1px solid #59af3f',
				'border-left':'1px solid #59af3f',
				'transform':'rotate(-45deg)'
			})
		}
	});
});
})(jQuery);

$(document).on('change', '#center, #city', function () {
		$('#center, #city').trigger('refresh');
});

$(document).on('change', '#part, #model', function () {
		$('#part, #model').trigger('refresh');
});

$('.selection__car').click(function(){
	if ($(this).attr('id') == 'octavia') {
		$('.filtration__img').prepend("<img src='img/img-octavia-big.png' alt='model' width='353' height='168'>");
	}

})

$('#btn-request').click(function(){
	$('.forms').css('display','block');
	$('#request').css('display','none');
	$('.page-footer').css('display','none');
})

$('#open-map').click(function(){
	$('body').append("<div class='overlay'></div>");
	$('#map').css('display','block');
})

$('#map-close').click(function(){
	$('.overlay').remove();
	$('#map').css('display','none');
})

window.addEventListener("keydown", function(event) {
	if (event.keyCode === 27) {
		$('.overlay').remove();
		$('#map').css('display','none');
	}
});

// range-mileage

$('#range-mileage').slider({
	animate: true,
  range: "min",
  value: 0,
  min: 0,
  max: 120000,
	step: 10,


	stop: function(event,ui) {
		$('#slider-result-mileage').val($('#range-mileage').slider('value'));
		$('#slider-result-mileage').attr('value', ui.value);
	},

  slide: function(event, ui) {
		$('#slider-result-mileage').val($('#range-mileage').slider('value'));
		$('#slider-result-mileage').attr('value', ui.value);

		$('#btn-count').removeClass('btn--green');
		$('#btn-count').attr('disabled');
		$('#mileage-message').css('display','none');
		$('#request').css('display','none');
		$('.forms').css('display','none');

		if (($('#slider-result-time').attr('value') > 0) && ($('#slider-result-mileage').attr('value') > 0)) {
			$('#btn-count').addClass('btn--green');
			$('#btn-count').removeAttr('disabled');
			$('#request').css('display','block');
		}

		if ($('#slider-result-mileage').attr('value') >= 120000) {
			$('#slider-result-mileage').val(120000);
			$('#mileage-message').css('display','block');
		}
  }

});

$('#slider-result-mileage').change(function(){
	$('#mileage-message').css('display','none');

	if (!$('#slider-result-mileage').val()) {
		$('#slider-result-mileage').val(0);
	}

	$('#btn-count').removeClass('btn--green');
	$('#btn-count').attr('disabled');
	$('#mileage-message').css('display','none');
	$('#request').css('display','none');
	$('.forms').css('display','none');

	if ($('#slider-result-mileage').val() >= 120000) {
		$('#slider-result-mileage').val(120000);
		$('#mileage-message').css('display','block');
	}

if (($('#slider-result-mileage').val() > 0) && ($('#slider-result-time').val() > 0)) {
		$('#btn-count').addClass('btn--green');
		$('#btn-count').removeAttr('disabled');
		$('#request').css('display','block');
}

	$('#range-mileage').slider('value',$('#slider-result-mileage').val());

})

// range-time

$('#range-time').slider({
	animate: true,
  range: "min",
  value: 0,
  min: 0,
  max: 116,
	step: 1,


	stop: function(event,ui) {
		$('#slider-result-time').val($('#range-time').slider('value'));
		$('#slider-result-time').attr('value', ui.value);
	},

  slide: function(event, ui) {
		$('#slider-result-time').val($('#range-time').slider('value'));
		$('#slider-result-time').attr('value', ui.value);

		$('#btn-count').removeClass('btn--green');
		$('#btn-count').attr('disabled');
		$('#request').css('display','none');
		$('.forms').css('display','none');

		if (($('#slider-result-time').attr('value') > 0) && ($('#slider-result-mileage').attr('value') > 0)) {
			$('#btn-count').addClass('btn--green');
			$('#btn-count').removeAttr('disabled');
			$('#request').css('display','block');
		}
  }

});

$('#slider-result-time').change(function(){
	if (!$('#slider-result-time').val()) {
		$('#slider-result-time').val(0);
	}

	if ($('#slider-result-time').val() >= 116) {
		$('#slider-result-time').val(116);
	}

	$('#btn-count').removeClass('btn--green');
	$('#btn-count').attr('disabled');
	$('#request').css('display','none');
	$('.forms').css('display','none');

	if (($('#slider-result-mileage').val() > 0) && ($('#slider-result-time').val() > 0)) {
			$('#btn-count').addClass('btn--green');
			$('#btn-count').removeAttr('disabled');
			$('#request').css('display','block');
	}

	$('#range-time').slider('value',$('#slider-result-time').val());

})


$('.range__input').keypress(function(event){
		var key, keyChar;
		if(!event) var event = window.event;

		if (event.keyCode) key = event.keyCode;
		else if(event.which) key = event.which;

		if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
		keyChar=String.fromCharCode(key);

		if(!/\d/.test(keyChar))	return false;

	});

	//styles - range

$('.ui-widget.ui-widget-content').css('border','none');
$('.ui-widget-header').css('background','transparent');
$('.ui-slider-handle').css({
	'width': '15px',
	'height': '13px',
	'background-color': '#2b2b2b',
	'border-radius': '2px',
	'cursor': 'pointer',
	'top': '6px'
})

// check e-mail pattern in form
$(document).ready(function(){
	var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
	var email = $('#email-1');

	email.blur(function(){
		if($(this).val() != '') {
			if(pattern.test($(this).val())) {
				$(this).css('background-color', '#ffffff');
			} else {
				$(this).css('background-color', '#ffe7e7');
			}
		}
	});
});

$(".labors__open").on("click", function(){
	$(this).toggleClass("labors__open--gray");
	$(this).parents(".labors__item").find(".labors__btn").toggleClass("labors__btn--open");
	$(this).parents(".labors__item").find(".labors__content").slideToggle();
});
